/**
 * @file
 * 
 * Functions to generate a calendar
 * 
 */
 
/**
 * Retrieves the number of days in a
 * given month. 
 * 
 * @param
 *   month integer
 * 
 * @param 
 *   year integer
 * 
 * @return
 *   integer
 * 
 */ 
function getDaysInMonth(month, year) {
  
  // Thirty days hath September,
  // April, June, and November;
  // All the rest have thirty-one,
  // Excepting February alone,
  // Which hath twenty-eight days clear,
  // And twenty-nine in each leap year.
  // The above rhyme can be used to calculate the 
  // number of days in the month

  // If month is Sept, Apr, Jun, Nov return 30 days.
  if (/8|3|5|10/.test(month)) return 30;

  // if month is not Feb return 31 days.
  if (month != 1) return 31;

  // To get this far month must be Feb ( 1 ).
  // if the year is a leap year then Feb has 29 days.
  if ((year % 4 == 0 && year % 100 != 0 ) || year % 400 == 0) return 29;

  // Not a leap year. Feb has 28 days.
  return 28;
}


// Getting the current date
var current_date = new Date();
var current_month = current_date.getMonth();
var current_year = current_date.getFullYear();

// Storing the days and months in an array
// as javascript date object portrays dates numerically
// and the following array helps to retrieve the
// day by mapping the date integer to the array key
var dayOfWeek = ['Sun','Mon','Tue','Wed', 'Thu', 'Fri', 'Sat'];
var monthOfYear = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

// We need to get the first day of the month
var firstDayOfMonth = new Date (current_year, current_month, 1);
firstDayOfMonth = firstDayOfMonth.getDay()
var noOfDaysInMonth = getDaysInMonth(current_month, current_year);
var lastDayOfMonth = noOfDaysInMonth + 1;
// This is the array that determines the calendar
// The first row is important as it determines the
// starting point of the calendar
var calArray = [0];
var day = 'NULL';
// This is to prevent the error
// Uncaught TypeError: Cannot set property '0' of undefined
for (var i = 0; i < 7; i++ ) {
  calArray[i] = new Array();
}

for (var j = 0; j < 6; j++) {
  for (var i = 0; i < 7; i++ ) {
    if (j === 0 && i === firstDayOfMonth && day === 'NULL'  ) {
      calArray[j][i] = 1;
      day  = 1;
      i++;
    }
    if ( day != 'NULL'  && day < lastDayOfMonth) {
      calArray[j][i] = day;
      day = ++day;
    }
  }  
}

/**
 *  Function to print the calendar.
 * 
 */
function printCalendar() {
  for (var j = 0; j < 5; j++) {
    for (var i = 0; i < 7; i++ ) {
      var id_value = '#day'+j+i;
      $(id_value).append(calArray[j][i]);
    }
  } 
}

$( document ).ready(function() {
  printCalendar();
});


console.log(calArray);
