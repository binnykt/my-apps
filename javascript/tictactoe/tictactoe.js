/* Javascript Tic Tac Toe


1. This is a game with only 2 users
2. The game consists of a sqaure grid with 3x3 squares
3. One user is assigned the X pattern, initially it is the user
4. The second user is assigned the O pattern
5. The objective of the game is to have a 3 adjacent squares with a simlar pattern. The adjacent squares can either be diagonal or 
6. The user whose pattern first meets the criteria is declared the winner and the game is considered to be finished
7. Only one user can add the pattern to a column at a given time and the next round is assigned to the next user
8. A pattern once added to a grid cannot be removed and stays there till the game is completed.


*/

// This variable determines who calls the shots
// By default this is 1 which means its user 1 who has the permission to
// add the pattern, user 2 is user 2 and user 0 is the logic of the program 
var token = 1;

// Determines whether this is a single player or multiplayer
var gameType = 1;

var winningPattern = 'NULL';

// Keeping track of the no of moves made in the game.
// Initial count = 0;
var count = 0;

// The Array data structure that holds the value of each grid
// Each row value corressponds to the grid on the board
// We start with Null to indicate that the square has no pattern within it.
var row = [];
row[3] = ['NULL','NULL','NULL'];
row[4] = ['NULL','NULL','NULL'];
row[5] = ['NULL','NULL','NULL'];

/*
 * This function is used to add the patterns to the class. 
 */
function addPattern(pattern, gridid, cclass, currenttoken) {
  
  // Updating the array data structure
  updatePatternRecord(gridid,pattern);
  var selectorid = '#'.concat(gridid);

  // switching the token to indicate the next players turn
  token = currenttoken;
  $(selectorid).html(pattern).fadeIn("slow");

  // Adding the class to indicate which grid has been accessed.
  $(selectorid).addClass(cclass);
  
   count++;
  // Finally checking if the game has been won by any player
  checkifWon();
}

/*
 *
 * Generating the random number for determnining 
 * the column
 */
function _generateRandom(from,to) {
    return Math.floor(Math.random()*(to-from+1)+from);
}

function _generateId(x, y){
 
  switch (x) {
    case 3:
      var gridid = 'a'.concat(y);
      break;
    case 4:
      var gridid = 'b'.concat(y);
      break;
    case 5:
      var gridid = 'c'.concat(y);
      break;
  }
   
  return gridid;
}

function _determineGrid() {
    x = _generateRandom(3,5);
    y = _generateRandom(0,2);
    

    // If the row is not empty we have to go for recursion
    row[x][y] = row[x][y];
    if (row[x][y] != 'NULL') {
      _determineGrid();
    }
    else {
      gridid = _generateId(x,y);
    }

    return gridid;
}

/*
 * The Artificial Intelligence of the game for single player games.
 *
 */
function notifyTictactoeAI() {
  // Checking if the AI program has the token otherwise it indicates that the other guy
  // calls the shots 
  if (token == 0) {
     var gridid = _determineGrid();
     addPattern('O', gridid, 'ai_was_here', 1);
     return true;
  }
}
// Update the array value corresponding to the currently selected id
function updatePatternRecord(gridid, pattern) {
   
  i =  gridid.substring(0,1);
  j =  gridid.substring(1);

  switch (i) {
   case 'a':
     i = 3;
     break;
   case 'b':
     i = 4;
     break;
   case 'c':
     i = 5;
     break;
   }
  
  // We need to do this as anything otherwise will cause an undefined error as row[i][j] was not declared 
  // previously in code
  row[i][j] = row[i][j];
  row[i][j] = pattern;
  // We need to check if the game has been won
  //checkifWon();
}

// We are calling both the functions here
function InitiateGame(gridid){
  // We need to prevent clicks on cells that 
  // were already assigned a pattern
  var selectedgrid = '#'.concat(gridid);
  if ($(selectedgrid).hasClass( "done" ) || $(selectedgrid).hasClass( "ai_was_here" )) {

    return false;
  }
  
  if (token == '1') {
    addPattern('X', gridid, 'done',0);
    notifyTictactoeAI();
  }
  
}

// Checks if a player has won
function checkifWon() {
  

   // Checking if the horizontal rows have a matching pattern
  for (i=3; i<=5; i++) {
     if (row[i][0] == row[i][1] && row[i][1] == row[i][2] && row[i][2] != 'NULL') {
      winningPattern = row[i][0];
     }
  }
  
  // Checking if the vertical columns have a matching pattern
  if (winningPattern == 'NULL') {
    for (i=0; i<=2; i++) {
      if (row[3][i] == row[4][i] && row[4][i] == row[5][i] && row[4][i] != 'NULL') {
        winningPattern = row[4][i];
      }
    }
  }

  // Checking if the diagonals have a match
  if (winningPattern == 'NULL') {
    if ((row[3][0] == row[4][1] && row[4][1] == row[5][2]) || (row[3][2] == row[4][1] && row[4][1] == row[5][0])) {
      winningPattern = row[4][1];
    }
  }

  if (winningPattern != 'NULL') {
     if (winningPattern == 'X') {
       alert("You have won");
       // Seting the token to prevent additional clicks after winning the game and indicating that 
       // the game has been won and a winner declared
       token = 5;
       return false;
     }
     else {
       token = 5;
       alert("You have lost");
       return false;
     }
  }

   // Check if the game is a draw based on the number of moves
   // made by both the AI and the human player together
   if (count == 9) {
     alert("Game is a draw");
     return false;
   }
}
// Our main function. This is where the action takes place
$(document).ready(function(){
   $("#tictactoe-board").click(function(event){
     if (token != 5) {
       var gridid = event.target.id;
       InitiateGame(gridid);
     }
   });
});


