// Initializing global Variables. 

var previous_square = 'NULL';
var initial_position = 'NULL';



function renderBoard() {
	// This will generate the board. A bit of fun animation before the battle ;-)
	
	
}

function resetMatch() {
	// This will reset the match for tired souls
	
}

function declareCheckmate() {
	//  I pity the guy who has to see this
	

}

function declareVictory() {
	// Do we need this?

}

function decideMove() {
	// This is the function for deciding the move by the computer player

  // We need to check the current valid move
}

function returnSquareId() {
	// Helper function for returning the square id. 
	
}

function returnPieceData() {
	//Get the Piece name and color
}

/**
 *  Checks whether the selected square has an image
 *  If it does have an image we add a class chess_selected to it
 *  We need to put this outside of this function
 */
function hasImage(square) {
  if($(square).attr('alt')) {
  	//alert(square);
    //image = $(square).attr('alt');
    $(square).addClass('chess_selected');
    //alert('has image');
    return 1;
 }
 else {
	//alert('No img');
	return 0;
	// Do nothing
 }
}
/**
 * Helper function to set the Initial position
 * before a piece is moved
 *
 */
function _setInitialPosition(value) {
  initial_position = value;
}
/**
 * Helper function to retrieve the Initial position
 * before a piece is moved
 *
 */
function _getInitialPosition() {
	return initial_position;
}
/**
 * Helper function to empty the Chess Board of the pieces
 *
 */
function _emptyChessBoard() {
  $('.piece').remove();
}
/**
 * Helper function to remove the chess selected class
 *
 */
function _removeChessSelected(square) {
	//alert("Hopefully removed the class");
	$('img').removeClass(".hess_selected");
}
/**
 * Function to move the chess piece
 *
 *
 */
function movePiece(square) {
  // Checking whether a square with an item has already been selected 
  // previously with the function given below. 
  if ($('.chess_selected').length > 0) {
  // Okay there seems to be a class called chess selected
  // which means that an column with an image has already been selected.  
  // Now we will consider this click to be on the destination square of the 
  // previous click

  // 1.Pass the id of the current square to variable final position
  // 2.Get the id of the class that is selected (May we should have passed it to a variable in the earlier function and pass the 
  // value to the variable the final position
  // 3. Pass the two values to the update position function
  // 5. After a successful move remove the class selected
    var initial_position = _getInitialPosition();
    var final_position = square;
    
    /** Test **/
    var piece_name = returnPiece(square);
    returnPieceArrayValue(piece_name);
    var test = returnSquare(square);
    //alert(test);
    
    /** Test end **/
    updatePiecePosition(initial_position,final_position);
    _setInitialPosition('NULL');
    _emptyChessBoard();
    fillChessBoard();
		
    return 1;
  }
  else {
  	//alert('no selected');
  	hasImage(square);
  	square = $(square).attr('alt');
  	_setInitialPosition(square);
  	
  	return 0;
  }	
}

function isValidMove(piece, initial_square, final_square) {
	// Get the piece name and color
	//getListofMoves(piece);
	returnMoveType(piece);
	// Compare it with the existing piece data

  //Now for the time being all moves are valid ;-)
  return 1;
}

function recordGame() {
	// This will record the whole moves in the game and convert it into
	// a portable standard format
	
}

function displaySquareId(current_square) {
	// This function will display the current id of the square which has been selected on 
	// a separate div
	
	// First we clear the writing area of any previous elementss      
  $('#meta-display').empty();
  $('#meta-display').append(current_square);
 
}

/*
 * Add functions that work on Click events here. 
 *
 */
function onBoardClick() {
	$(".column").click(function(event) {
		
	  var current_square = event.target.id;
	  gameDebug(current_square);
	 
	  // Retrieving the id of the square which is currently 
	  // selected
	  var current_square  = '#'.concat(current_square);
	  var squareColor = $(current_square).attr("class");
    	 	
    selectSquare(current_square, squareColor);
    movePiece(current_square);
    
  });
}
/**
 * Debugging function to print values in a selected area
 */ 
function gameDebug(value) {
  $('#debug').empty();
	$('#debug').append(value);

}

function _setPrevSquare(value) {
  previous_square = value;
}

function _getPrevSquare(variable) {
	return previous_square;
}

/*
 * Displays the Selected square
 * 
 */
function selectSquare(current_square, squareColor) {
	// Before we change the background color we need to do two things
	// Find out the color of the current square so that we can change things accordingly
	// and also reset the color when it is unclicked
	// Next we need to reset the color when another square is clicked
	
  var previous_square = _getPrevSquare();
  gameDebug(previous_square);
  if (current_square != previous_square && previous_square != 'NULL') {
	  // Remove the background color of the previous square and let it use 
	  // the stylesheet color
	  $(previous_square).css('background-color','');
  }
  
  // Now we need to set the background color of the select square 
  // depending on its present color. 
  if(squareColor == 'square-white') {	
	  bg_color = '#FFA959';
	}
	else {
		bg_color = '#AF733A';
	}
	
	$(current_square).css('background-color', bg_color);

	
	// First the we get the current background color and the current square
	_setPrevSquare(current_square);

  
}

/*
 * Adds the pieces to the square on the start of the Game
 *
 *
 */
function fillChessBoard() {
	$('.column').children().each(function () {
    current_square = this.id;
    current_square = '#'.concat(current_square); 
    addChessPiece(current_square);
  });
	
}

/*
 * This is the function that adds the chess pieces to the squares in the board
 * when the square is clicked referring to the array representation of the board
 */
function addChessPiece(current_square) {
	
	// Get the piece for the particular square
	piece = returnPiece(current_square);

	
	image = returnPieceImgSrc(piece);
	imagepath = 'pieces/' + image;
	
	if (image != 'NULL') {
		  
	  $(current_square).append('<img class="piece" id='+piece+' alt='+current_square+' src='+imagepath+'>');
  }
 
  // We also need to figure out how to remove the piece from the previous square
}

///**************************************** Here we start the execution of the actual code ****************************************//

$( document ).ready(function() {
	fillChessBoard();
	onBoardClick();
});

 
