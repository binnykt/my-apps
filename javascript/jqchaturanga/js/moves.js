/**
 * @file
 * 
 * Stores the moves of each piece
 *
 *
 * 
 * 
 * 
 *
 *
 *
 */
 

/*
  1. First we need to determine the color of the piece so as to differentiate between pieces that go up and pieces that go down: The black pieces go down and the white pieces go up
  2. Next we need to determine the xy coordinates of the initial position of the piece
  3. Then we need to determine th xy coordinates of the destination position of the piece
  4. Then we simulate the actual movement by calculating through the move and determine
    4.a. The pattern of movement - straight, diagonal
    4.b. The number of squares moved
  5. We compare the information received and then determine whether it is a valid move for that piece
  6. End of story hopefully
*/

// We need a data structure to store the movements of each piece and then we need to translate them into the board position values
// 
// We can classify the movements of every chess piece into 
// Infinite columns any - The Queen
// Infinite columns diagonal - The Bishop
// Infinite columns straight - The Rook
// Single column any - King
// Fixed moves Shape - Knight
// Single column multiple - Pawn
var whitemove = [];
var blackmove = [];
// For all white pieces
whitemove[0] = ['WQ'];
whitemove[1] = ['WB-1', 'WB-2'];
whitemove[2] = ['WR-1', 'WR-2'];
whitemove[3] = ['WK'];  
whitemove[4] = ['WN-1', 'WN-2'];
whitemove[5] = ['WP-1', 'WP-2', 'WP-3', 'WP-4', 'WP-5','WP-6','WP-7','WP-8'];

// For all black pieces
blackmove[0] = ['BQ'];
blackmove[1] = ['BB-1', 'BB-2'];
blackmove[2] = ['BR-1', 'BR-2'];
blackmove[3] = ['BK'];
blackmove[4] = ['BN-1', 'BN-2'];
blackmove[5] = ['BP-1','BP-2','BP-3','BP-4','BP-5','BP-6','BP-7','BP-8'];

// Now we need to translate these moves into its equivalent board representation
function returnMoveType(piece_name) {
	
	// locate piece in array 
	// return key type
	var move_type = move.indexOf(piece_name);
	

}
/**
 *
 *
 *
 */
function returnInitXY() {
	var initxy = [];
	
	//return initxy;
}

/**
 *
 *
 *
 */
function returnDestXY() {
	var destxy = [];
	
	//return destxy;
}
/** 
 * returns the color of the piece to determine the direction of its movement
 *
 */
function returnPiececolor(piece_name) {
	
}

/**
 *  Function to decide the move pattern of the array
 *
 */
function calculateMovePattern(initialxy, destxy) {
	//  Split initial coordinates
	var xinit = initialxy[0];
	var yinit = initialxy[1];
	
	
	//  Split destination coordinates
  var xdest = destxy[0];
  var ydest = destxy[1]; 
  
  // Compare xinit and xdest
  // Compare yinit and ydest
  
  // Straight movements
  if(xinit == xdest){
    if(yinit < ydest) {
    	return 'top';
    }
    else {
      return 'bottom';
    }
  }
  if(yinit == ydest){
  	 
  }
  
}