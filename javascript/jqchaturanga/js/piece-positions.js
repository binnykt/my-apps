/*
 *  @file
 *
 * The variables currently stores the pieces in the squares
 * This is the data structure that stores the information regarding the
 * pieces and the squares
 *
 *
 *
 */
 
// The default pieces of each of the rows in the beginning
var row = [];
row[1] = ['BR-1', 'BN-1', 'BB-1', 'BQ',   'BK',   'BB-2', 'BN-2', 'BR-2'];
row[2] = ['BP-1', 'BP-2', 'BP-3', 'BP-4', 'BP-5', 'BP-6', 'BP-7', 'BP-8'];
row[3] = ['NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL'];
row[4] = ['NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL'];
row[5] = ['NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL'];
row[6] = ['NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL'];
row[7] = ['WP-1', 'WP-2', 'WP-3', 'WP-4', 'WP-5', 'WP-6', 'WP-7', 'WP-8'];
row[8] = ['WR-1', 'WN-1', 'WB-1', 'WQ',   'WK',   'WB-2', 'WN-2', 'WR-2'];

var boardValues = ['A','B','C','D','E','F','G','H'];	
/*
 * Translates the machine names of the images into the actual images themselves
 * 
 *
 */ 
function returnPieceImgSrc(piece_code) {
	piece = piece_code;
	piece_clean = piece.substring(0,2);
	

	switch(piece_clean) {
	  case 'WP':
     return 'white_pawn.png';
  
    case 'BP':
     return 'black_pawn.png';

    case 'BR':
     return 'black_rook.png';

    case 'WR':
     return 'white_rook.png';

    case 'BN':
     return 'black_knight.png';

    case 'WN':
     return 'white_knight.png';

    case 'WB':
     return 'white_bishop.png';

    case 'BB':
     return 'black_bishop.png';

    case 'WQ':
     return 'white_queen.png';

    case 'BQ':
     return 'black_queen.png';

    case 'WK':
     return 'white_king.png';

    case 'BK':
     return 'black_king.png';
    
    case 'NU':
      return 'NULL';
    
  }
	
}

/*
 *
 * Takes in the current active square as an argument and returns the 
 * chess piece that is currently placed in it or if the square is empty 
 * returns 'NULL'
 *
 */ 
function returnPiece(current_square) {
  
  // This is the array that stores the numerical value corresponding to the row alphabet
  // to help in easier calculation. A is the 1st column and D is the 4th column

	
	// Remove the # from the current_square to make to clean
	// before continuing the rest of the processing
	current_square_clean = current_square.substring(1);
  
  // Now we need to get the row number from the current square value.
  // Eg. '3' from C-3 and add 'row' to from row3 which is the name of the 
  // array holding the value of the piece
  current_row = current_square_clean.substring(2);
  
  // Now we need to get the value of the column using the Array above as 
  // reference  
  column = current_square_clean.substring(0,1);
	column = boardValues.indexOf(column);
	
	// As we now have the column and the row value we need to splice them together to form the 
	// array name Eg row5[6]
  piece = row[current_row][column];
	return piece;

}

function returnSquare(square) {
  
  var boardArrayRep = [];
  //Removing the # from the string and cleaning it  
  square_clean = square.substring(1);
  boardArrayRep[0] = square_clean.substring(2);
  column = square_clean.substring(0,1);
 	boardArrayRep[1] = boardValues.indexOf(column);

return boardArrayRep;
}

/*
 * When a piece is moved on the table we need to update in the array data structure.
 *
 */
function updatePiecePosition(initial_position,final_position) {
//  Locate the column of the piece and the initial position
//var selected_piece = returnPiece(initialposition);
// Save the value found in the initial position
// Next set the value to NULL;
  var final_pos_vect = 'NULL';
  var initial_pos_vect = returnSquare(initial_position);
  var x = initial_pos_vect[0];
  var y = initial_pos_vect[1];
  
  var removed_piece = row[x][y];
  row[x][y] = 'NULL';
 
  var final_pos_vect = returnSquare(final_position);
  var a = final_pos_vect[0];
  var b = final_pos_vect[1];
  
  row[a][b] = removed_piece;
}

/**
 * Returns the array name of the destination of the piece
 *
 *
 */
function returnPieceArrayValue(piece_name) {
  if(piece_name = 'NULL') {
    return 0;
  }
  // locate the piece in the array 

  for(var i=0; i<=8; i++) {
     var piece_position = jQuery.inArray(piece_name, row[i]);
     // return the value.
     if (piece_position != -1) {
        alert(piece_position);
        return 1;
     }
     
  } 
   
 
 
}

/**
 * Decides the movement of h
 *
 */