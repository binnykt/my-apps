#!/bin/bash

# A bash script to organize loose files based on their type
# 
# This script will loop through all files in the current directory
# get its extension and place them in a folder with that extension
#
#
# Author: Binny K. Thomas
#########################################333

IFSOLD=$IFS
IFS=$'\n'
echo "Processing all files"
echo "<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>"
pwd
echo "<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>"
#
# Looping through all the files in the directory
#
for files in $(ls) 
do
  # Checking whether this is a file or directory
  if [ -f $files ]
    then
    # If it is a file we get its extension
   
    filename=$(basename $files)
    extension="${filename##*.}"
    filename="${filename%.*}"
    # We are going to create a directory to store these files
    # The directory will be named according to this extension
    # in a particular naming convention like fcb_<extension>
    
    # So first we need to check if these directories exist
    # in a previous run of the program
    fcdir=fcb_$extension
    if [[ ! -d $fcdir ]]
      then
      # If the directory does not exist we make that custom directory
      mkdir $fcdir
    fi
  fi
  
done

# Getting the value of the current script
currentfile=$(basename $0)

for files in $(ls)
do
  if [ -f $files ] 
    then
      # We need to ensure that the current script is not removed in the process
      if [[ $files != $currentfile ]]
        then
         #We need to get the extensions once again
         filename=$(basename $files)
         extension="${filename##*.}"
         filename="${filename%.*}"
         mv $files fcb_$extension
      fi
  fi
done
IFS=$IFSOLD
