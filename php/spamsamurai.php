<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en" id="vbulletin_html">
<head>
</head>
<body>
<?php
  
  // The list of all alphanumeric characters in the English language
  $character_bin = 'abcdedfghiklmnopqrstuvwxyzABCDEDFGHIJKLMNOPQRSTUVWXYZ';
  
  //Generate random text
  function generate_random_text() {
    global $character_bin;
    
    $seed = rand(5,20);
    $random_text = '';
    for($i = 0; $i < $seed; $i++) { 
      $random_text .= $character_bin[rand(0, strlen($character_bin) - 1)];
    }
    
    return $random_text;
  }  
  // Generate random links.
  //print_r($_SERVER);
  for($j = 0; $j < 5; $j++) {
     echo '<a href="' . $_SERVER['SCRIPT_NAME'] . '/' . generate_random_text(); 
     echo '">' . generate_random_text(); 
     echo '</a>';
     echo '<br >';
  }


 
?>
</body>
</html>
