(function ($) {
  Drupal.behaviors.watchbar = {
    attach: function (context, settings) {
    	var message = Drupal.settings.watchbar.message;
      var count = Drupal.settings.watchbar.count;
      var state = false;
      if (count == 0 ) {
        state = true;
      }
      $(function() {
          $.jBar({
              startClosed: state,
              type: 'fixed', // fixed/static (lowercase)
              delay: '1000', // In milliseconds
              backgroundColor: '#DB5903', // Background Color
              borderColor: '#FFF', // Background Color
              buttonTextColor: '#FFF', // Button Text
              buttonColor: '#333', // Button Color
              buttonColorHover: '#222', // Button Color Hover
              calltoAction: message,
              buttonText: 'Click to investigate',
              buttonLink: '?q=admin/reports/dblog' // Hyperlink from button
          });
      });
    }
  };
}(jQuery));
