<?php

/**
 * @file
 * Contains the admin settings form of the module.
 */

/**
 * The admin settings form of the module
 */
function watchbar_settings_form() {
  $form['severity_type'] = array(
    '#title' => t('Severity Type'),
    '#type'  => 'fieldset',
    '#description' => t('Select the severity type of the messages you want to be displayed on the bar'),
  );
  $form['severity_type']['severity_type_boxes'] = array(
    '#title' => t('Watchdog Severity Levels'),
    '#type'  => 'radios',
    '#default_value' => variable_get('watcbar_severity', array('0')),
    '#options' => array(
        '0' => 'Emergency',
        '1' => 'Alert',
        '2' => 'Critical',
        '3' => 'Error',
        '4' => 'Warning',
        '5' => 'Notice',
        '6' => 'Info',
        '7' => 'Debug',
    ),
  );
  $form['granularity'] = array(
    '#title' => t('Notification Age'),
    '#type'  => 'fieldset',
    '#description' => t('Select the age of the messages you want to be displayed on the bar'),
  );
  $form['granularity']['time_limit_boxes'] = array(
    '#title' => t('Granularity'),
    '#type'  => 'radios',
    '#default_value' => variable_get('watchbar_time_limit_value', array('5')),
    '#options' => array(
        '1'  => 'Day',
        '7'  => 'Week',
        '14' => '2 Weeks',
        '30' => 'Month',
     ),
  );
  $form[] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  
  return $form;
}

/**
 * The submit handler of the form
 */
function watchbar_settings_form_submit($form, &$form_state) {
  $watchbar_severity = $form_state['values']['severity_type_boxes'];
  $watchbar_time_limit_value = $form_state['values']['time_limit_boxes'];
  
  variable_set('watcbar_severity', $watchbar_severity);
  variable_set('watchbar_time_limit_value', $watchbar_time_limit_value); 
  
  drupal_set_message(t("The settings have been saved"));
}
